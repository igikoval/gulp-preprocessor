'use strict';
var through = require('through2');

String.prototype.match_from = function(regexp, pos) {
    var text = this.substr(pos);
    var matches = this.match(regexp);

    return matches;
}

var preprocessor = function (text) {
    this.text = text;
    this.defines = {};
    this.pos = 0;

    return this;
}


preprocessor.prototype.parse = function() {
    var cmd;

    while ((cmd = this.text.match(/(?:^|\s)\#([^\s]+)(?:$|\n|\s([^\n]*))?/)) !== null) {
        var line = cmd[0];

        var pos = this.text.indexOf(line);

        if (typeof this['p_' + cmd[1]] !== "undefined") {
            if (this.text[pos-1] == '/' && this.text[pos-2] == '/') {
                this.text = this.text.substr(0, pos - 2) + this.text.substr(pos + line.length);
            } else {
                this.text = this.text.substr(0, pos) + this.text.substr(pos + line.length);
            }
            this['p_' + cmd[1]](cmd[2], pos);
        } else
            console.log('p_' + cmd[1]);

    }

    return this;
}


preprocessor.prototype.toString = function() {
    return this.text;
}


preprocessor.prototype.toBuff = function () {
    return new Buffer(this.text);
};


preprocessor.prototype.p_define = function(text, pos) {
    var cmd = text.match(/([^\s]+)\s*(.*)/);

    if (cmd.length === 0)
        return;

    if (typeof cmd[2] === "undefined")
        cmd[2] = true;

    this.defines[cmd[1]] = cmd[2];


    var regexp = new RegExp('\\s\\#define\\s+' + cmd[1] +'\\s+(.+)', 'i');

    var redef_match = this.text.match(regexp);
    var redef_pos = (redef_match !== null) ? this.text.indexOf(redef_match[0]) : false;


    while ((pos = this.text.indexOf(cmd[1], pos)) > -1 && (redef_pos === false || pos < redef_pos)) {

        var ndef = true;
        var mspace = false;
        for (var i = pos; i >= 0; i--) {
            if (this.text[i] === "\n" || (this.text[i] === " " && (mspace != mspace) === false))
                break;
            else if (this.text[i] === "#") {
                this.ndef = false;
                break;
            }
        }

        if (ndef)
            this.text = this.text.substr(0, pos) + cmd[2] + this.text.substr(pos + cmd[1].length);
    }
}


preprocessor.prototype.p_ifdef = function(text, pos) {
    var cmd = text.match(/([^\s]+)\s/);
    console.log(text);
    console.log(cmd);

    if (cmd === null)
        return this.cut_to_endif(pos);

    if (typeof this.defines[cmd[1]] === "undefined") {
        return this.cut_to_endif(pos);
    }

}


preprocessor.prototype.p_ifndef = function(text, pos) {
    var cmd = text.match(/([^\s]+)\s/);

    if (cmd === "null")
        return;

    if (typeof this.defines[cmd[1]] !== "undefined") {
        this.cut_to_endif(pos);
    }
}


preprocessor.prototype.p_if = function (text, pos) {
    var cmd = text.match(/([^\s]+)\s+([<>=]+)\s*([^\s]+)/);

    if (cmd === null)
        return this.cut_to_endif(pos);

    var ift = false;


    if (cmd[2] === ">")
        if (cmd[1] > cmd[3])
            return this.cut_else(pos-1);

    if (cmd[2] === "<=")
        if (cmd[1] <= cmd[3])
            return this.cut_else(pos-1);

    if (cmd[2] === "<")
        if (cmd[1] < cmd[3])
            return this.cut_else(pos-1);

    if (cmd[2] === ">=")
        if (cmd[1] >= cmd[3])
            return this.cut_else(pos-1);

    if (cmd[2] === "=" || cmd[2] === "==" || cmd[2] === "===")
        if (cmd[1] == cmd[3])
            return this.cut_else(pos-1);

    this.cut_to_endif(pos-1);

};


preprocessor.prototype.p_else = function () {
    return;
};


preprocessor.prototype.p_endif = function () {
    return;
};


preprocessor.prototype.cut_to_endif = function(pos, lpos) {
    var e_match = this.text.match_from(/\s\#endif\s/i, (typeof lpos !== "undefined") ? lpos : pos);
    var el_match = this.text.match_from(/\s\#else\s/i, (typeof lpos !== "undefined") ? lpos : pos);

    if (e_match === null) {
        this.text = this.text.substr(0, pos);
    } else {
        var end_pos = this.text.indexOf(e_match[0], (typeof lpos !== "undefined") ? lpos : pos);
        if (el_match !== null) {
            var else_pos = this.text.indexOf(el_match[0], (typeof lpos !== "undefined") ? lpos : pos);
            if (else_pos < end_pos) {
                this.text = this.text.substr(0, pos) + this.text.substr(else_pos + ' #else '.length);
                return;
            }
        }

        this.text = this.text.substr(0, pos) + this.text.substr(end_pos + ' #endif '.length);
    }

}


preprocessor.prototype.cut_else = function(pos, lpos) {
    var e_match = this.text.match_from(/\s\#endif\s/i, (typeof lpos !== "undefined") ? lpos : pos);
    var el_match = this.text.match_from(/\s\#else\s/i, (typeof lpos !== "undefined") ? lpos : pos);

    if (e_match === null || el_match === null)
        return;

    var end_pos = this.text.indexOf(e_match[0], (typeof lpos !== "undefined") ? lpos : pos);
    var else_pos = this.text.indexOf(el_match[0], (typeof lpos !== "undefined") ? lpos : pos);

    if (else_pos > end_pos)
        return;

    this.text = this.text.substr(0, else_pos) + this.text.substr(end_pos + ' #endif '.length);
}

module.exports = function() {
    return through.obj(function(file, encoding, callback){
        var text = String(file.contents);

        file.contents = new preprocessor(text).parse().toBuff();

        callback(null, file);
    });
}
